"""
simple issue tracker module

@copyright: (c) 2011 hackmeeting U{http://sindominio.net/hackmeeting}
@author: Ruben Pollan
@organization: hackmeeting U{http://sindominio.net/hackmeeting}
@contact: meskio@sindominio.net
@license:
  This program is free software; you can redistribute it and/or
  modify it under the terms of the Do What The Fuck You Want To
  Public License, Version 2, as published by Sam Hocevar. See
  U{http://sam.zoy.org/projects/COPYING.WTFPL} for more details.
"""

from emma.events import Event, subscribe, trigger
from emma.module import Module
from emma.complement import use_lock
from emma.interface.message import Message
from pymongo import ASCENDING


class issues(Module):
    def run(self):
        self.issue_list = {}
        help_event = Event(event="help", identifier=self.conf['id'])
        subscribe(help_event, self.help_handler)
        cmd_event = Event(event="command", identifier=self.conf['id'])
        subscribe(cmd_event, self.cmd_handler)

    def help_handler(self, event, data):
        if not data:
            return _("  Keep track of issues.\n"
                     "  * issues [group]\n"
                     "    Show issues. If group given, filter by it.\n"
                     "  * issue group bla\n"
                     "    Add new issue, required by the group\n"
                     "  * done n\n"
                     "    close issue numbered with n in issue list.\n")
        elif data[0] in ('issues', _('issues')):
            return _("Show the list of issues.")
        elif data[0] in ('issue', _('issue')):
            return _("Add new issue")
        elif data[0] in ('done', _('done')):
            return _("Close the issue'")
        else:
            return ""

    def cmd_handler(self, event, data):
        cmd, args = data[0]
        if cmd in ("issues", _("issues")):
            self.issues(event, data)
        elif cmd in ("issue", _("issue")):
            self.issue(event, data)
        elif cmd in ("done", _("done")):
            self.done(event, data)

    def get_to(self, event, data):
        to = data[1]['From']
        if event.interface == 'irc' and data[1]['To'][0] == '#':
            to = data[1]['To']
        return to

    def reply(self, event, data, message):
        to = self.get_to(event, data)
        msg = Message(message, to)
        event = Event(event="send", interface=event.interface,
                      identifier=event.identifier)
        trigger(event, msg)

    def issue(self, event, data):
        cmd, args = data[0]
        if event.interface == "email":
            group, text = args.split("\n", 1)
        else:
            group, text = args.split(" ", 1)
        self.db.insert({"group": group, "text": text})

        self.reply(event, data, _("issue accepted"))
        self.log(_("Issue accepted (%(group)s, %(text)s)") % {'group': group, 'text': text})

    def issues(self, event, data):
        cmd, args = data[0]
        if args:
            issue_list = self.db.find({'group': args})
            message = _('Issues of %s group:\n' % args)
        else:
            issue_list = self.db.find()
            message = _('Issues:\n')
        issue_list.sort('group', ASCENDING)

        to = self.get_to(event, data)
        issue_list = [issue for issue in issue_list]
        self.store_issue_list(issue_list, to)
        issue_template = "\n %(number)4d - %(group)s: %(text)s"
        counter = 0
        for issue in issue_list:
            message += issue_template % {'number': counter,
                                         'group': issue['group'],
                                         'text': issue['text']}
            counter += 1

        self.reply(event, data, message)

    def done(self, event, data):
        cmd, args = data[0]
        to = self.get_to(event, data)
        issue_list = self.issue_list[to]
        try:
            index = int(args)
        except ValueError:
            self.reply(event, data, _("invalid issue number"))
        try:
            self.db.remove(issue_list[index])
            self.reply(event, data, _("Issue done"))
        except IndexError:
            self.reply(event, data, _("invalid issue number"))

    @use_lock
    def store_issue_list(self, issue_list, to):
        self.issue_list[to] = issue_list
